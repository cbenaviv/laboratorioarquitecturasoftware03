

/**
 *
 * @author Carlos Benavidez
 */
public class CalculadoraFigura2 extends java.rmi.server.UnicastRemoteObject implements CalculadoraFiguras{
    
   
    CalculadoraFigura2()throws java.rmi.RemoteException
    {
        
    }
    
 
    public long area(Figura figura) throws java.rmi.RemoteException
    {
        return figura.area();
    }
    

   
    public long volumen(Figura figura) throws java.rmi.RemoteException
    {
        return figura.volumen();
    }
    
}
