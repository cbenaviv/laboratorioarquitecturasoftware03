
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos Benavidez
 */
public class ClienteRMI {

    public static void main(String[] args) throws NotBoundException, MalformedURLException, RemoteException {

        try {

            final CalculadoraFiguras c1 = (CalculadoraFiguras) Naming.lookup("rmi://localhost/CalculadoraService");
            String[] figuras = {"Seleccione Figura", "Cilindro", "Cubo", "Esfera"};

            final JComboBox jcd = new JComboBox(figuras);

            JButton boton = new JButton("Calcular");

            boton.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {

                    String figura = (String) jcd.getSelectedItem();

                    if (!figura.equals("Seleccione Figura")) {
                        int opcion = jcd.getSelectedIndex();
                        switch (opcion) {
                            
                            case 1:
                                //Cilindro
                                
                                try {
                                    
                                    String radio = JOptionPane.showInputDialog("Ingrese radio cilindro");
                                    String altura = JOptionPane.showInputDialog("Ingrese altura cilindro");
                                    
                                    if (radio != null && altura != null) {
                                        long r = Long.parseLong(radio);
                                        long a = Long.parseLong(altura);
                                        
                                        Cilindro cilindro = new Cilindro(r, a);
                                        
                                        System.out.println("Area Cilindro:" + c1.area(cilindro));
                                        System.out.println("Volumen Cilindro:" + c1.volumen(cilindro));
                                        JOptionPane.showMessageDialog(null, "Area de un cilindro de radio: " + radio + " altura: " + altura + " es: " + c1.area(cilindro));
                                        JOptionPane.showMessageDialog(null, "Volumen de un cilindro de radio: " + radio + " altura: " + altura + " es: " + c1.volumen(cilindro));
                                        
                                    } else {
                                        
                                        JOptionPane.showMessageDialog(null, "No  ingreso informacion completa", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                    
                                } catch (Exception ex) {
                                    
                                    JOptionPane.showMessageDialog(null, "No  ingreso informacion valida", "Error", JOptionPane.ERROR_MESSAGE);
                                    
                                }
                                
                                break;
                                
                            case 2:
                                //Cubo
                                
                                try {
                                    
                                    String lado = JOptionPane.showInputDialog("Ingrese un lado del Cubo");
                                    
                                    if (lado != null) {
                                        long l = Long.parseLong(lado);
                                        Cubo cubo = new Cubo(l);
                                        System.out.println("Area Cubo:" + c1.area(cubo));
                                        System.out.println("Volumen Cubo:" + c1.volumen(cubo));
                                        
                                        JOptionPane.showMessageDialog(null, "Area de un cubo de lado: " + l+ " es: " + c1.area(cubo));
                                        JOptionPane.showMessageDialog(null, "Volumen de un cubo de lado: " + l+" es : "+ c1.volumen(cubo));
                                        
                                        
                                        
                                    } else {
                                        JOptionPane.showMessageDialog(null, "No  ingreso informacion completa", "Error", JOptionPane.ERROR_MESSAGE);

                                    }
                                    
                                } catch (Exception ex) {
                                    
                                    JOptionPane.showMessageDialog(null, "No  ingreso informacion valida", "Error", JOptionPane.ERROR_MESSAGE);
                                    
                                }
                                
                                break;
                                
                            case 3:
                                //Esfera
                                
                                try {
                                    
                                    String radioEsfera = JOptionPane.showInputDialog("Ingresese radio de la esfera");
                                    if (radioEsfera != null) {
                                        
                                        long re = Long.parseLong(radioEsfera);
                                        Esfera esfera = new Esfera(re);
                                        System.out.println("Area Esfera:" + c1.area(esfera));
                                        System.out.println("Volumen Esfera:" + c1.volumen(esfera));
                                        
                                        JOptionPane.showMessageDialog(null, "Area de la esfera de radio: " + re+ " es: " + c1.area(esfera));
                                        JOptionPane.showMessageDialog(null, "Volumen de una  esfera de radio: " + re+" es : "+ c1.volumen(esfera));
                                       
                                        
                                    }
                                    else
                                    {
                                        JOptionPane.showMessageDialog(null,"No ingreso informacion completa","Error",JOptionPane.ERROR_MESSAGE);
                                    }
                                    
                                } catch (Exception ex) {
                                    
                                    JOptionPane.showMessageDialog(null, "No  ingreso informacion valida", "Error", JOptionPane.ERROR_MESSAGE);
                                    
                                    
                                }
                                
                                break;
                                
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Fallo");
                    }

                }
            });

            jcd.setEditable(true);

            Object[] options = new Object[]{};
            JOptionPane jop = new JOptionPane("Calculadoras Figura",
                    JOptionPane.QUESTION_MESSAGE,
                    JOptionPane.DEFAULT_OPTION,
                    null, options, null);

            jop.add(jcd);

            jop.add(boton);

            JDialog diag = new JDialog();
            diag.getContentPane().add(jop);
            diag.pack();
            diag.setVisible(true);

            int opciones = 1;

        } catch (Exception e) {
            System.out.println("Problema:" + e);
        }

    }

}
