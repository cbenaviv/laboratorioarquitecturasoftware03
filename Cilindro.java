
/**
 *
 * @author Carlos Benavidez
 */
public class Cilindro extends java.rmi.server.UnicastRemoteObject implements Figura {

    private long radio;
    private long altura;

    Cilindro(long radio, long altura) throws java.rmi.RemoteException {
        this.radio = radio;
        this.altura = altura;
    }

    @Override
    public long area()throws java.rmi.RemoteException{
        long areaLateral = (long) (2*Math.PI)*altura;
        long areaBase=(long) ((long) Math.PI*Math.pow(radio, 2));
        long areaTotal=areaLateral+areaBase;

        return areaTotal;
    }

    @Override
    public long volumen()throws java.rmi.RemoteException{
        long pi=(long)Math.PI;
        long radio2=(long) Math.pow(radio,2);
        
        
        return pi*radio2*altura;
    }

}
