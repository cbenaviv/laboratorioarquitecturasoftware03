
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.Naming;
import java.rmi.Remote;

/**
 *
 * @author Carlos Benavidez
 */
public  class ServidorFigura {
    
    
    public ServidorFigura() throws RemoteException, MalformedURLException
    {
       try
       {
            CalculadoraFiguras c=new CalculadoraFigura2();    
            Naming.rebind("rmi://localhost:1099/CalculadoraService", (Remote) c);

       }

       catch(Exception e)
       {
          System.out.print("Problema:"+e);
       }
       
       
    }
    
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        new ServidorFigura();
    }
    
}
