
/**
 *
 * @author Carlos Benavidez
 */
public class Esfera extends java.rmi.server.UnicastRemoteObject implements Figura {

    private long radio;
    
    
    Esfera(long radio) throws java.rmi.RemoteException
    {
        this.radio=radio;
    }
    
    
  @Override
   public long volumen() throws java.rmi.RemoteException{
       long a=4/3;
       long b=(long) Math.PI;
       
       
       return (long) (a*b*Math.pow(this.radio,3));
    }
   
   
   @Override
    public long area() throws java.rmi.RemoteException{
   
        
        return (long) (4*Math.PI*Math.pow(this.radio,2));

    }
    
    
  

}
