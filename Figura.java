
/**
 *
 * @author Carlos Benavidez
 */
public interface Figura extends java.rmi.Remote{
    
       
    public long volumen()throws java.rmi.RemoteException;
    public long area() throws java.rmi.RemoteException;

    
}
