



public interface CalculadoraFiguras extends java.rmi.Remote{
    
    public long area(Figura figura) throws java.rmi.RemoteException;
    public long volumen(Figura figura)throws java.rmi.RemoteException;
    
    
}
