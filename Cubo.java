

/**
 *
 * @author Carlos Benavidez
 */
public class Cubo extends java.rmi.server.UnicastRemoteObject implements Figura {

    private long lado;

    Cubo(long lado) throws java.rmi.RemoteException{
        this.lado = lado;
    }

    @Override
    public long area() throws java.rmi.RemoteException{
        return (long) (6*Math.pow(lado, 2));
    }

    @Override
    public long volumen()throws java.rmi.RemoteException{
        return (long) Math.pow(lado,3);
    }
    
   
    

}
